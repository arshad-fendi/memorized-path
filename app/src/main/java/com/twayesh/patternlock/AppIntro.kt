package com.twayesh.patternlock

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.github.appintro.AppIntro
import com.github.appintro.AppIntroFragment


class AppIntro : AppIntro() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Make sure you don't call setContentView!

        this.supportActionBar?.hide()

        // Call addSlide passing your Fragments.
        // You can use AppIntroFragment to use a pre-built fragment
        addSlide(AppIntroFragment.newInstance(
                title = "You plan a trip",
                description = "Going to the gym is very simple",
                imageDrawable = R.drawable.intro1
        ))
        addSlide(AppIntroFragment.newInstance(
                title = "Know the path",
                description = "You remember the turns by heart",
                imageDrawable = R.drawable.intro2
        ))
        addSlide(AppIntroFragment.newInstance(
                title = "Imagine drawing your path",
                description = "The path becomes your secret",
                imageDrawable = R.drawable.intro3
        ))
        addSlide(AppIntroFragment.newInstance(
                title = "Memorized Path",
                description = "And that's how you preserve your secrets",
                imageDrawable = R.drawable.intro4
        ))
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        // Decide what to do when the user clicks on "Skip"
        finishActivity()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        // Decide what to do when the user clicks on "Done"
        finishActivity()
    }

    private fun finishActivity() {
        val resultIntent = Intent()
        resultIntent.putExtra(MainActivity.RESULT_KEY, "seen")
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }
}