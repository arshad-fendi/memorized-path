package com.twayesh.patternlock;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    public static boolean exitOnBack = false;
    public static final int ACTIVITY_RESULT = 0;
    public static final String RESULT_KEY = "RESULT_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public static void showAboutDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(R.string.about_title)
                .setCancelable(true)
                .setMessage(R.string.about_text)
                .setPositiveButton(R.string.about_ok, null);

        final SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.shared_pref), Context.MODE_PRIVATE);

        final CheckBox agree = new CheckBox(context);
        agree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sharedPreferences.edit().putBoolean(context.getString(R.string.data_collection_agreement), b).apply();
            }
        });
        agree.setChecked(sharedPreferences.getBoolean(context.getString(R.string.data_collection_agreement), false));
        agree.setText(R.string.about_agreement);
        builder.setView(agree);

        builder.show();

    }

    public static void showDataCollectionDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(R.string.data_collection_title)
                .setCancelable(true)
                .setMessage(R.string.data_collection_text)
                .setPositiveButton(R.string.about_ok, null);

        final SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.shared_pref), Context.MODE_PRIVATE);

        final CheckBox agree = new CheckBox(context);
        agree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sharedPreferences.edit().putBoolean(context.getString(R.string.data_collection_agreement), b).apply();
            }
        });
        agree.setText(R.string.about_agreement);
        builder.setView(agree);

        builder.show();

    }

    @Override
    public void onBackPressed() {
        if(exitOnBack) {
            finish();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.myNavHostFragment);
        return navController.navigateUp();
    }
}