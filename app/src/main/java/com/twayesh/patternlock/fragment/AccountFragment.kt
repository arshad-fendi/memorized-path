package com.twayesh.patternlock.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.twayesh.patternlock.AppIntro
import com.twayesh.patternlock.MainActivity
import com.twayesh.patternlock.R
import com.twayesh.patternlock.databinding.FragmentAccountBinding

class AccountFragment : Fragment() {

    private lateinit var sharedPref: SharedPreferences
    private var seenOnboarding = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPref = requireContext().getSharedPreferences(getString(R.string.shared_pref), Context.MODE_PRIVATE)
        seenOnboarding = sharedPref.getBoolean(getString(R.string.seen_onboarding), false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentAccountBinding>(inflater,
                R.layout.fragment_account, container, false)

        MainActivity.exitOnBack = true
//        val flags = View.SYSTEM_UI_FLAG_LOW_PROFILE or
//                View.SYSTEM_UI_FLAG_FULLSCREEN or
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or // Removes the action bar
//                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
//                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
//                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//        activity?.window?.decorView?.systemUiVisibility = flags
//        (activity as? AppCompatActivity)?.supportActionBar?.hide()

        (activity as? AppCompatActivity)?.supportActionBar?.show()


        binding.newProfile.setOnClickListener{view : View ->

            if(seenOnboarding) {
                view.findNavController().navigate(R.id.action_accountFragment_to_newPathFragment)
            } else {
                val intent = Intent(context, AppIntro::class.java)
                startActivityForResult(intent, MainActivity.ACTIVITY_RESULT)
            }
        }

        setHasOptionsMenu(true)

        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        (activity as? AppCompatActivity)?.title = getString(R.string.app_name)

        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == MainActivity.ACTIVITY_RESULT && resultCode == Activity.RESULT_OK) {
            val seen = data?.getStringExtra(MainActivity.RESULT_KEY);
            if(seen?.isNotEmpty()!!) {
                if(!seenOnboarding) {
                    sharedPref.edit().putBoolean(getString(R.string.seen_onboarding), true).apply()
                    findNavController().navigate(R.id.action_accountFragment_to_newPathFragment)
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.account_option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menu_how_to) {
            val intent = Intent(context, AppIntro::class.java)
            startActivity(intent)
        } else if(item.itemId == R.id.menu_about) {
            MainActivity.showAboutDialog(requireContext())
        }

        return super.onOptionsItemSelected(item)
    }
}