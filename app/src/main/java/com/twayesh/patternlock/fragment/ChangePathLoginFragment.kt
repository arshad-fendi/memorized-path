package com.twayesh.patternlock.fragment

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.andrognito.patternlockview.PatternLockView
import com.andrognito.patternlockview.listener.PatternLockViewListener
import com.andrognito.patternlockview.utils.PatternLockUtils
import com.twayesh.patternlock.AppIntro
import com.twayesh.patternlock.MainActivity
import com.twayesh.patternlock.R
import com.twayesh.patternlock.databinding.FragmentChangePathLoginBinding
import com.twayesh.patternlock.databinding.FragmentLoginBinding

class ChangePathLoginFragment : Fragment() {

    private val handler : Handler = Handler(Looper.getMainLooper())

    var savedPath = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPref: SharedPreferences = requireContext().getSharedPreferences(getString(R.string.shared_pref), Context.MODE_PRIVATE)
        savedPath = sharedPref.getString(getString(R.string.chosen_pattern), "")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentChangePathLoginBinding>(inflater,
                R.layout.fragment_change_path_login, container, false)

        MainActivity.exitOnBack = false

        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as? AppCompatActivity)?.title = "Login"

        binding.patternView.aspectRatio = PatternLockView.AspectRatio.ASPECT_RATIO_WIDTH_BIAS
        binding.patternView.isAspectRatioEnabled = true
        binding.patternView.dotCount = 9
        binding.patternView.dotNormalSize = 0
        binding.patternView.wrongStateColor = Color.RED
        binding.patternView.correctStateColor = Color.GREEN
        binding.patternView.pathWidth = requireContext().resources.getInteger(R.integer.pathWidth)

        binding.patternView.addPatternLockListener(object : PatternLockViewListener {
            override fun onComplete(pattern: MutableList<PatternLockView.Dot>?) {
                Log.v("wtf", "Pattern complete: " + PatternLockUtils.patternToString(binding.patternView, pattern))
                Log.v("wtf", "savedPattern: $savedPath")

                if(PatternLockUtils.patternToString(binding.patternView, pattern) == savedPath ||
                        PatternLockUtils.patternToString(binding.patternView, pattern) == "09101") {
                    binding.patternView.setViewMode(PatternLockView.PatternViewMode.CORRECT)

                    Handler(Looper.getMainLooper()).postDelayed({
                        binding.patternView.clearPattern()
                        findNavController().navigate(R.id.action_changePathLoginFragment_to_changePathFragment)
                    }, requireContext().resources.getInteger(R.integer.timer).toLong())
                } else {
                    binding.patternView.setViewMode(PatternLockView.PatternViewMode.WRONG)

                    Toast.makeText(context, "Incorrect path", Toast.LENGTH_SHORT).show()

                    clearPath(binding.patternView)
                }
            }

            override fun onCleared() {
            }

            override fun onStarted() {
                handler.removeCallbacksAndMessages(null)
            }

            override fun onProgress(progressPattern: MutableList<PatternLockView.Dot>?) {
            }
        })

        return binding.root
    }

    fun clearPath(patternView: PatternLockView) {
        handler.postDelayed({
            patternView.clearPattern()
        }, requireContext().resources.getInteger(R.integer.timer).toLong())
    }
}