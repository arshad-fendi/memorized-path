package com.twayesh.patternlock.fragment

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.twayesh.patternlock.AppIntro
import com.twayesh.patternlock.MainActivity
import com.twayesh.patternlock.R
import com.twayesh.patternlock.databinding.FragmentUnlockedBinding

class UnlockedFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentUnlockedBinding>(inflater,
                R.layout.fragment_unlocked, container, false)

        MainActivity.exitOnBack = true

        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        (activity as? AppCompatActivity)?.title = "Password Store"

//        val flags = View.SYSTEM_UI_FLAG_LOW_PROFILE or
//                View.SYSTEM_UI_FLAG_FULLSCREEN or
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
//                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
//                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
//                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//        activity?.window?.decorView?.systemUiVisibility = flags
//        (activity as? AppCompatActivity)?.supportActionBar?.hide()

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.unlocked_option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_change_password -> {
                findNavController().navigate(R.id.action_unlockedFragment_to_changePathLoginFragment)
            }
            R.id.menu_how_to -> {
                val intent = Intent(context, AppIntro::class.java)
                startActivity(intent)
            }
            R.id.menu_about -> {
                MainActivity.showAboutDialog(requireContext())
            }
        }

        return super.onOptionsItemSelected(item)
    }
}