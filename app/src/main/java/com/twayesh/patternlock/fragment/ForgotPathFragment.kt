package com.twayesh.patternlock.fragment

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.twayesh.patternlock.AppIntro
import com.twayesh.patternlock.MainActivity
import com.twayesh.patternlock.R
import com.twayesh.patternlock.databinding.FragmentForgotPathBinding

class ForgotPathFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentForgotPathBinding>(inflater,
                R.layout.fragment_forgot_path, container, false)

        MainActivity.exitOnBack = false

        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as? AppCompatActivity)?.title = "Reset path"

        binding.sendEmail.setOnClickListener{
            val email = binding.editText.text
            if(email.isNotEmpty() && email.contains("@")) {
                binding.confirmation.visibility = View.VISIBLE
                binding.editText.isEnabled = false
            } else {
                Toast.makeText(context, getString(R.string.invalid_email), Toast.LENGTH_SHORT).show()
            }
        }

        return binding.root
    }
}