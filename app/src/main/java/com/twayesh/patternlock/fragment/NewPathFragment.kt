package com.twayesh.patternlock.fragment

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Resources
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.andrognito.patternlockview.PatternLockView
import com.andrognito.patternlockview.listener.PatternLockViewListener
import com.andrognito.patternlockview.utils.PatternLockUtils
import com.twayesh.patternlock.AppIntro
import com.twayesh.patternlock.MainActivity
import com.twayesh.patternlock.R
import com.twayesh.patternlock.databinding.FragmentNewPathBinding
import kotlin.math.log

class NewPathFragment : Fragment() {

    private val handler : Handler = Handler(Looper.getMainLooper())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentNewPathBinding>(inflater,
                R.layout.fragment_new_path, container, false)

        MainActivity.exitOnBack = false

        var chosenPattern = ""
        var isFirstPasswordSet = false

        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as? AppCompatActivity)?.title = "Register new path"

        binding.buttonLogin.setOnClickListener{ view : View ->
            view.findNavController().navigate(R.id.action_newPathFragment_to_loginFragment)
        }

        binding.editText.addTextChangedListener {
            text ->
            if(text?.contains("@")!! && text?.contains(".")!!) {
                binding.textNewPattern.visibility = View.VISIBLE
                binding.lines.visibility = View.VISIBLE
                binding.patternView.visibility = View.VISIBLE
            }
        }

        binding.patternView.aspectRatio = PatternLockView.AspectRatio.ASPECT_RATIO_WIDTH_BIAS
        binding.patternView.isAspectRatioEnabled = true
        binding.patternView.dotCount = 9
        binding.patternView.dotNormalSize = 0
        binding.patternView.wrongStateColor = Color.RED
        binding.patternView.correctStateColor = Color.WHITE
        binding.patternView.pathWidth = requireContext().resources.getInteger(R.integer.pathWidth)

        binding.patternView.addPatternLockListener(object : PatternLockViewListener {
            override fun onComplete(pattern: MutableList<PatternLockView.Dot>?) {
                if(pattern!!.size < 10) {
                    Toast.makeText(context, getString(R.string.path_too_short), LENGTH_SHORT).show()
                    clearPath(binding.patternView)
                } else if(!isFirstPasswordSet) {
                    isFirstPasswordSet = true
                    chosenPattern = PatternLockUtils.patternToString(binding.patternView, pattern)

                    binding.textNewPattern.text = getString(R.string.confirm_new_path)
                    clearPath(binding.patternView)
                } else if(PatternLockUtils.patternToString(binding.patternView, pattern) == chosenPattern) {
                    binding.patternView.setViewMode(PatternLockView.PatternViewMode.CORRECT)
                    binding.patternView.correctStateColor = Color.GREEN
                    binding.patternView.isInputEnabled = false

                    binding.textNewPattern.text = "Correct path, you may now login"
                    binding.buttonLogin.visibility = View.VISIBLE
                    (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)

                    clearPath(binding.patternView)

                    // Save path to shared preferences
                    val sharedPref: SharedPreferences = requireContext().getSharedPreferences(getString(R.string.shared_pref), Context.MODE_PRIVATE)
                    val editor = sharedPref.edit()
                    editor.putString(getString(R.string.chosen_pattern), PatternLockUtils.patternToString(binding.patternView, pattern)).apply()
                } else {
                    binding.patternView.setViewMode(PatternLockView.PatternViewMode.WRONG)

                    Toast.makeText(context, getString(R.string.incorrect_path), LENGTH_SHORT).show()

                    clearPath(binding.patternView)
                }
            }

            override fun onCleared() {
            }

            override fun onStarted() {
                handler.removeCallbacksAndMessages(null)
            }

            override fun onProgress(progressPattern: MutableList<PatternLockView.Dot>?) {
            }
        })

        setHasOptionsMenu(true)

        return binding.root
    }

    fun clearPath(patternView: PatternLockView) {
        handler.postDelayed({
            patternView.clearPattern()
        }, requireContext().resources.getInteger(R.integer.timer).toLong())
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.new_path_option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menu_how_to) {
            val intent = Intent(context, AppIntro::class.java)
            startActivity(intent)
        } else if(item.itemId == R.id.menu_about) {
            MainActivity.showAboutDialog(requireContext())
        }

        return super.onOptionsItemSelected(item)
    }
}