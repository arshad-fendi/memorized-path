package com.twayesh.patternlock.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.andrognito.patternlockview.PatternLockView
import com.twayesh.patternlock.AppIntro
import com.twayesh.patternlock.MainActivity
import com.twayesh.patternlock.R
import com.twayesh.patternlock.databinding.FragmentAccountBinding
import com.twayesh.patternlock.databinding.FragmentSplashBinding

class SplashFragment : Fragment() {

    private lateinit var sharedPref: SharedPreferences
    private var savedPath = ""
    private var seenDataCollection = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPref = requireContext().getSharedPreferences(getString(R.string.shared_pref), Context.MODE_PRIVATE)
        savedPath = sharedPref.getString(getString(R.string.chosen_pattern), "")!!
        seenDataCollection = sharedPref.getBoolean(getString(R.string.seen_data_collection), false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentSplashBinding>(inflater,
                R.layout.fragment_splash, container, false)

        MainActivity.exitOnBack = false
//        val flags = View.SYSTEM_UI_FLAG_LOW_PROFILE or
//                View.SYSTEM_UI_FLAG_FULLSCREEN or
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or // Removes the action bar
//                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
//                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
//                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//        activity?.window?.decorView?.systemUiVisibility = flags
        (activity as? AppCompatActivity)?.supportActionBar?.hide()

        endSplashScreen()

        return binding.root
    }



    private fun endSplashScreen() {
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            if(savedPath == "") {
                findNavController().navigate(R.id.action_splashFragment_to_accountFragment)
            } else {
                findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
            }

            if(!seenDataCollection) {
                sharedPref.edit().putBoolean(getString(R.string.seen_data_collection), true).apply()
                MainActivity.showDataCollectionDialog(requireContext())
            }
        }, requireContext().resources.getInteger(R.integer.splash_timer).toLong())
    }
}