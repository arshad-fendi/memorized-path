# What is Memorized Path?
It is a new method of authenticating and accessing sensitive information on your phone Utilizing the picture superiority effect - the mind’s ability to more accurately remember pictures compared to words - to allow for more complex and secure passcodes.

![Example](img/tutorial.png)

Use Memorized Path locking mechanism to secure your ordinary passcodes.

# Run Memorized Path

Memorized Path reuires devices and emulator with minimum API 21 (Android 5.0 Lollipop)

Clone the project and wait for Gradle to completly build the project

Disclaimer: Memorized Path has only been tested on a device with a screen density 402 ppi (OnePlus 5t).

## On an emulator
* Start your android emulator
* Run

## On a devices
* Connect your phone
* Run

## Install APK
* Download memorized_path.apk from APK folder
* Connect your device or start your android emulator
* Use following ADB command to install
```
    adb install <path>/memorized_path.apk
```

## Overview of app
![Example](img/App_Overview.png)


# More information
Project homepage has more information about how Memorized Path was formed and background research.
* http://memorized_route.twayesh.com/ 